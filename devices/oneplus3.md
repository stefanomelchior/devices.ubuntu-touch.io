---
codename: 'oneplus3'
name: 'Oneplus 3'
comment: 'community device'
icon: 'phone'
image: 'https://wiki.lineageos.org/images/devices/oneplus3.png'
maturity: .8
---

### Device specifications

|    Component | Details                                                               |
|-------------:|-----------------------------------------------------------------------|
|      Chipset | Qualcomm MSM8996 Snapdragon 820                                       |
|          CPU | Quad-core (2x2.15 GHz Kryo & 2x1.6 GHz Kryo)                          |
| Architecture | arm64                                                                 |
|          GPU | Adreno 530                                                            |
|      Display | 5.5 in 1920 x 1080                                                    |
|      Storage | 64GB                                                                  |
|       Memory | 4 GB                                                                  |
|      Cameras | 16 MP, LED flash<br>8 MP, No flash                                    |
|   Dimensions | 152.7 mm (6.01 in) H 74.7 mm (2.94 in) W 7.35 mm (0.289 in) D         |

### Port status
|         Component | Status | Details            |
|------------------:|:------:|--------------------|
|          AppArmor |    Y   |                    |
|      Boot into UI |    Y   |                    |
|        Bluetooth  |    Y   |                    |
|            Camera |    Y   | Requires gst-droid |
|    Cellular Calls |    Y   |                    |
|     Cellular Data |    Y   |                    |
|       Fingerprint |    N   |                    |
|               GPS |    Y   |                    |
|           Sensors |    Y   |                    |
|             Sound |    Y   |                    |
| UBPorts Installer |    Y   |                    |
|  UBPorts Recovery |    Y   |                    |
|          Vibrator |    Y   |                    |
|             Wi-Fi |    Y   |                    |


### Maintainer(s)
Vanyasem, Mariogrip, Vince1171, Ernes_t

### Forum topic
https://forums.ubports.com/topic/3253/oneplus-3-3t

### Source repos
https://github.com/Halium/android_device_oneplus_oneplus3
https://github.com/Halium/android_kernel_oneplus_msm8996

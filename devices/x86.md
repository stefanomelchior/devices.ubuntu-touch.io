---
codename: 'x86'
name: 'Desktop PC'
comment: 'experimental'
icon: 'tv'
image: 'https://unity8.io/img.png'
noinstall: true
maturity: .1
---

## Desktop PCs

You can install [Lomiri](https://unity8.io), the desktop environment of Ubuntu Touch previously known as Unity 8, on all debian-derived Linux distributions, including as Ubuntu.

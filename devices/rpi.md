---
codename: 'rpi'
name: 'Raspberry Pi'
comment: 'experimental'
icon: 'tv'
noinstall: true
maturity: .1
---

The [Raspberry Pi](https://www.raspberrypi.org/) is an affordable ARM-based linux single-board computer. An [experimental Ubuntu Touch image](https://ci.ubports.com/job/rootfs/job/rootfs-rpi/) is available. You can find [installation instructions on GitLab](https://gitlab.com/ubports/community-ports/raspberrypi).

---
codename: 's3ve3g'
name: 'Samsung S3 Neo'
comment: 'wip'
icon: 'phone'
noinstall: true
maturity: .05
---

A community port has been started [here](https://forums.ubports.com/topic/3652/call-for-testing-samsung-s3-neo-s3ve3g-owners).

### Working

 - Actors: Manual brightness
 - Actors: Notification LED
 - Actors: Torchlight
 - Actors: Vibration
 - Cellular: Carrier info, signal strength
 - Cellular: Data connection
 - Cellular: Incoming, outgoing calls
 - Cellular: PIN unlock
 - Cellular: SMS in, out
 - Cellular: Speakerphone (limited volume control)
 - Cellular: Voice in calls (limited volume control)
 - GPU: Boot into UI
 - Misc: Offline charging
 - Misc: Online charging
 - Misc: Shutdown / Reboot
 - Network: Bluetooth
 - Network: WiFi
 - Network: Flight mode
 - Sensors: Automatic brightness
 - Sensors: GPS
 - Sensors: Proximity
 - Sensors: Rotation
 - Sensors: Touchscreen
 - Sound: Earphones
 - Sound: Loudspeaker
 - Sound: Microphone
 - Sound: Volume control
 - USB: RNDIS access

### Working with manual steps

 - Network: Hotspot (needs reboot after end of hotspot mode, does not pair with any WiFi anymore)

### Not working

 - Camera: Flashlight
 - Camera: Photo
 - Camera: Video
 - Cellular: MMS in, out
 - GPU: Video acceleration
 - USB: MTP access

---
codename: 'zuk_z2_plus'
name: 'Zuk z2 Plus'
comment: 'wip'
icon: 'phone'
noinstall: true
maturity: .05
---

A community port has been started [here](https://forums.ubports.com/topic/3829/zuk-z2_plus).

### Working

 - Audio
 - Calling
 - SMS
 - 4G
 - Wifi (sometimes requires a reboot)
 - GPS
 - Vibration
 - Orientation sensor
 - Anbox
 - Libertine
 - camera
 - Notification Light

### Not working

 - Video recording/decoding
 - Bluetooth
 - Flash light
 - ADB/MTP
 - camera (recording)
 - fingerprint

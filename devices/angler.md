---
codename: 'angler'
name: 'Nexus 6P'
comment: 'wip'
icon: 'phone'
noinstall: true
image: 'https://wiki.lineageos.org/images/devices/angler.png'
maturity: .15
---

A port has been started [here](https://forums.ubports.com/topic/3678/call-for-testing-google-huawei-nexus-6p-angler-owners).

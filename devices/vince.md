---
codename: 'vince'
name: 'Xiaomi Redmi 5 Plus'
comment: 'wip'
icon: 'phone'
noinstall: true
maturity: .8
---


### Device specifications
Basic   | Spec Sheet
-------:|:----------
CPU     | Octa-core 2.02 GHz ARM Cortex A53
Chipset | Qualcomm Snapdragon 625, MSM8953
GPU     | Adreno 506
ROM     | 32/64GB 
RAM     | 3/4GB
Android | 7.1.2
Battery | 4000 mAh
Display | 1080x2160 pixels, 5.99
Rear Camera  | 12MP, PDAF
Front Camera | 5 MP

### Port status
|         Component | Status | Details            |
|------------------:|:------:|--------------------|
|          AppArmor |    Y   |                    |
|      Boot into UI |    Y   |                    |
|            Camera |    N   |                    |
|    Cellular Calls |    Y   |                    |
|     Cellular Data |    Y   |                    |
|               GPS |    Y   |                    |
|           Sensors |    Y   |                    |
|             Sound |    Y   |                    |
| UBPorts Installer |    N   |                    |
|  UBPorts Recovery |    N   |                    |
|          Vibrator |    Y   |                    |
|             Wi-Fi |    Y   |                    |
|         Bluetooth |    Y   |                    |

### Maintainer(s)
BirdZhang, Bartek

### Contributor(s)
Asdew 

### Source repos
https://github.com/ubports-on-vince

### CI builds
https://github.com/ubports-on-vince/ubports-ci
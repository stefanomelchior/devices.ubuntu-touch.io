---
codename: 'rolex'
name: 'Xiaomi Redmi 4(A)'
comment: 'wip'
icon: 'phone'
image: 'http://cdn2.gsmarena.com/vv/pics/xiaomi/xiaomi-redmi-4a-3.jpg'
noinstall: true
maturity: .3
---

### Device specifications

|    Component | Details                                                               |
|-------------:|-----------------------------------------------------------------------|
|      Chipset | Qualcomm MSM8917 Snapdragon 425                                       |
|          CPU | Quad-core 1.4 GHz Cortex-A53                                          |
| Architecture | arm64                                                                 |
|          GPU | Adreno 308                                                            |
|      Display | 720x1280                                                              |
|      Storage | 16 GB / 32 GB                                                 |
|       Memory | 2 GB / 3 GB                                               |
|      Cameras | 13 MP, LED flash<br>5 MP, No flash                                    |
|   Dimensions |139.9 x 70.4 x 8.5 mm.          |

### Port status
|         Component | Status | Details            |
|------------------:|:------:|--------------------|
|          AppArmor |    Y   |                    |
|      Boot into UI |    Y   |                    |
|            Camera |    Y   | Requires gst-droid |
|    Cellular Calls |    Y   |                    |
|     Cellular Data |    Y   |                    |
|               GPS |    Y   |                    |
|           Sensors |    Y   |                    |
|             Sound |    Y   |                    |
| UBPorts Installer |    N   |                    |
|  UBPorts Recovery |    Y   |                    |
|          Vibrator |    Y   |                    |
|             Wi-Fi |    Y   |                    |

### Maintainer(s)
TEA

### Forum topic
https://forum.xda-developers.com/redmi-4a/development/ubuntu-touch-rolex-redmi4a-t4058619

### Source repos
https://github.com/areyoudeveloper1

### CI builds
https://github.com/areyoudeveloper1/ubports-ci

### Notes:
To download artifact need login github account
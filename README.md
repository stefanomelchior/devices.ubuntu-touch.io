# Ubuntu Touch devices

The new new [Ubuntu Touch devices website](https://devices.ubuntu-touch.io)! Made using [Gridsome](https://gridsome.org/). You can start a development server by running `gridsome develop`.

## Adding new devices

You can add new devices by adding a markdown file with a YAML-metadata-block under `/devices/<codename>.md`.

The YAML-metatdata-block should have the following format; variables in square brackets are `[optional]`:

```yml
name: <marketing name of the device>
codename: <primary codename of the device>
icon: <phone|tablet-landscape|laptop|tv>
comment: <displayed in the device list, eg "community device">
maturity: <number between 0 and 1 indicating quality of the port>
[video: <video embed url>]
[image: <image src url for the detail page>]
[noinstall: <set to true to hide the installer block>]
```

The markdown-part of the file should be a short description of the device, possibly including some important links. Make sure you don't go above heading-level `h2`.

```markdown
## A cool device with a catchy tagline

This particular device is especially interesting, because it has a **markdown** description, that will be rendered on the detail page.
```

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
